package com.samsara.team

import com.samsara.team.logger.logger
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.gson.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.routing.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    install(ContentNegotiation) {
        gson(ContentType.Application.Json)
    }

    routing {
        trace { application.log.trace(it.buildText()) }

        post("/api/v1/meeting") {
            logger.trace { "Before call.receiveText()" }
            val text = call.receiveText()
            logger.trace { "After call.receiveText()" }
        }
    }
}

